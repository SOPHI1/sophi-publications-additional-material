# Sophi publications additional material

- ``SOPHI FDT and SDO HMI cross-comparison`` contains additional material from the paper: "Comparison of magnetic data products from Solar Orbiter SO/PHI-FDT and SDO/HMI, A.Moreno-Vacas, et al., Astronomy and Astropysics, 2024."